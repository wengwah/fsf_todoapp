// Always use an IIFE, i.e., (function() {})();
(function () {
    angular
        .module("todoApp") // to call an angular module, omit the second argument ([]) from the angular.module() syntax
        // this is known as the getter syntax
        .controller("TodoCtrl", TodoCtrl); // angular.controller() attaches a controller to the angular module
                                           // specified as you can see, angular methods are chainable

    // Dependency injection. Empty [] means TodoCtrl does not have dependencies
    TodoCtrl.$inject = [];

    function TodoCtrl() {
        var vm = this;

        // Initialize variables,
        // t0do is array for unfinished items
        vm.todo = [];
        // donetodo is an array for finished items
        vm.doneTodo = [];
        vm.newTodo = {};

        // List of exposed functions
        // All callable/public functions are called at the top, giving you a quick view of functionalities provided by
        // the controller
        vm.remove = remove;
        vm.submit = submit;
        vm.changeStatus = changeStatus;

        // ====
        // Function declaration and definition
        // These are private functions and become accessible only when exposed (attached to the vm object) as above

        // Function called when we want to remove to-do items
        // doneStatus identifies which array the element (in this case object) is to be removed
        // index identifies the array element that we want removed
        // index is passed from the calling html, in this case index.html
        function remove(doneStatus, index) {
            // This if statement checks the doneStatus to determine the array the element is in
            if(doneStatus) {
                // splice is a method available to the array object
                // it is used to add to or remove from an array
                // below, we're saying, starting from index, remove 1 element from the todos array
                vm.doneTodo.splice(index, 1);
            }
            else{
                vm.todo.splice(index, 1);
            }
        };

        // Function called when we want to add to-do items
        function submit() {
            // push is a method available to the array object; it adds one or more element to the end of the array
            // below we're saying add the object newTodo to the end of the t0do array (the array for unfinished items)
            vm.todo.push(vm.newTodo);
            vm.newTodo = {};

        };

        // Function called when we complete or un-complete an item
        function changeStatus(doneStatus, index) {
            // This if statement checks the doneStatus to determine the array the element is in
            if(!doneStatus) {
                // This splice method removes the item from the t0do array, and then returns it in an array
                // The concat method adds this array to the end of the done t0do method
                // We set doneT0do to this new combined array
                vm.doneTodo = vm.doneTodo.concat(vm.todo.splice(index, 1));
            }
            else {
                vm.todo = vm.todo.concat(vm.doneTodo.splice(index, 1));
                }
        };
    }
})();